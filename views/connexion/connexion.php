<section class="connexion">
        <h2>Identification Client</h2>
        <p>Merci d'entrer votre identifiant et votre mot de passe pour acceder à votre espace client.</p>
        <form class="connection-Form" action="<?= BASEURL?>/index.php/connexion/connexion" method="POST">
          <input type="text" class="form-control-lg" name="username" placeholder="Pseudo">
          <input type="password" class="form-control-lg" name="password"  placeholder="Mot de passe">
          <button class="btn btn-large btn-primary btn-lg" type="submit" name="connection" value="connection">Connexion</button>
        </form>
</section>