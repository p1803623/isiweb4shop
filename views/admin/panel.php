<section class="panel">
    <h2>ADMIN PANEL</h2>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">id</th>
            <th scope="col">delivery_add_id</th>
            <th scope="col">payment_type</th>
            <th scope="col">date</th>
            <th scope="col">status</th>
            <th scope="col">session</th>
            <th scope="col">total</th>
            <th scope="col">Detail</th>

            </tr>
        </thead>
        <tbody>
            <?php
            foreach($data['orders'] as $order){
                ?>
                    <tr>
                    <th scope="row"><?= $order['id'] ?></th>
                    <td><?= $order['delivery_add_id'] ?></td>
                    <td><?= $order['payment_type'] ?></td>
                    <td><?= $order['date'] ?></td>
                    <td><?= $order['status'] ?></td>
                    <td><?= $order['session'] ?></td>
                    <td><?= $order['total'] ?></td>
                    <?php
                        if($order['status'] == 2){
                            ?>
                            <td><a href="<?= BASEURL ?> /index.php/admin/index/<?= $order['id'] ?>">Detail</a></td>
                            <?php
                        }
                        else{
                            ?>
                            <td>[en attente]</td>
                            <?php
                        }
                    ?>
                    </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    
    <?php
    if(isset($data['order'])){
        ?>
        <table class='items'>
            <thead>
                <tr>
                    <th scope="col">product_id</th>
                    <th scope="col">quantity</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach($data['order']['items'] as $item){
                        ?>
                        <tr>
                            <td scope="row"><?= $item['product_id'] ?></td>
                            <td><?= $item['quantity'] ?></td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th scope="col">firstname</th>
                    <th scope="col"> lastname</th>
                    <th scope="col">add1</th>
                    <th scope="col">city</th>
                    <th scope="col">postcode</th>
                    <th scope="col">phone</th>
                    <th scope="col">email</th>
                </tr>        
            </thead>
            <tbody>
                <tr>
                    <td><?= $data['order']['address']['firstname'] ?></td>
                    <td><?= $data['order']['address']['lastname'] ?></td>
                    <td><?= $data['order']['address']['add1'] ?></td>
                    <td><?= $data['order']['address']['city'] ?></td>
                    <td><?= $data['order']['address']['postcode'] ?></td>
                    <td><?= $data['order']['address']['phone'] ?></td>
                    <td><?= $data['order']['address']['email'] ?></td>
                </tr> 
            </tbody>
        </table>

        <a class="btn btn-large btn-primary btn-lg" href="<?= BASEURL ?>/index.php/panier/validation/<?= $data['order']['items'][0]['order_id'] ?>">Valider La commande</a>
        <?php
    }
    ?>
    <a href="<?= BASEURL ?> /index.php/admin/deconnexion">Deconnexion</a>
</section>