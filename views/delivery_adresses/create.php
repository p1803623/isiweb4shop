<section class="create-address">
        <h2>Votre adresse </h2>
        <p>Merci d'entrez votre adresse avant de passez à l'étape suivante</p>
        <form class="address-form" action="<?= BASEURL?>/index.php/delivery_addresses/add" method="POST">
          <input type="text" class="form-control-lg" name="prenom" placeholder="prenom">
          <input type="text" class="form-control-lg" name="nom"  placeholder="Nom">
          <input type="text" class="form-control-lg" name="address1"  placeholder="Adresse">
          <br>
          <input type="number" class="form-control-lg" name="postcode"  placeholder="code Postal">
          <input type="text" class="form-control-lg" name="city"  placeholder="ville">
          <br>
          <input type="number" class="form-control-lg" name="phone"  placeholder="N° Telephone">
          <input type="email" class="form-control-lg" name="email"  placeholder="email">
          <button class="btn btn-large btn-primary btn-lg" type="submit" name="confirmer" value="confirmer">Confirmer</button>
        </form>
</section>