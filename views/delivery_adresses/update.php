<section class="update-address">
    <p>votre adresse actuelle est : </p>
    <?php
    ?>
     <form class="address-form" action="<?= BASEURL?>/index.php/delivery_addresses/update" method="POST">
          <input type="text" class="form-control-lg" name="prenom" value="<?= $data['firstname'] ?>">
          <input type="text" class="form-control-lg" name="nom"  value="<?= $data['lastname'] ?>">
          <input type="text" class="form-control-lg" name="address1"  value="<?= $data['add1'] ?>">
          <input type="number" class="form-control-lg" name="postcode"  value="<?= $data['postcode'] ?>">
          <input type="text" class="form-control-lg" name="city"  value="<?= $data['city'] ?>">
          <input type="number" class="form-control-lg" name="phone"  value="<?= $data['phone'] ?>">
          <input type="email" class="form-control-lg" name="email"  value="<?= $data['email'] ?>">
          <button class="btn btn-large btn-primary btn-lg" type="submit" name="confirmer" value="modifier">Modifier</button>
    </form>
</section>