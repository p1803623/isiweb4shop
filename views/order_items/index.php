<section class="order_items">
    <h2>Votre panier</h2>
    <?php
    foreach($data as $order_item){
        ?>
        <div class="order_item">
            <img src="<?= BASEURL ?>/assets/productimages/<?= $order_item['image']?>" alt="image produit">
            <div class="text">
                <p>Nom : <?= $order_item['name']?></p>
                <p>Prix: <?= $order_item['price']?></p>
                <p>Quantité: <?= $order_item['quantity']?></p>
                <p>Total : <?= $order_item['quantity']*$order_item['price']?></p>
                <a href="<?= BASEURL?>/index.php/order_items/delete/<?= $order_item['id'] ?>">Retirer</a>/
                <a href="<?= BASEURL?>/index.php/order_items/add/<?= $order_item['id'] ?>">Ajouter un élément</a>
            </div>
        </div>
        <br>
        <?php
    }

    if($data != null){
        ?>
        <a href="<?= BASEURL ?>/index.php/delivery_addresses"  class="btn btn-large btn-primary btn-lg">Confirmer</a>
        <?php
    }
    ?>
</section>