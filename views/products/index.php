<section class="products">
    <?php
        foreach($data as $product){
            ?>
            <div class="product">
                <img src="<?= BASEURL .'/assets/productimages/' . $product['image'] ?> " alt="<?= $product['name'] ?>">
                <div class = "text">
                    <h3><?= $product['name'] ?></h3>
                    <p><?= $product['description'] ?></p>
                    <strong>Notre prix : <?= $product['price'] ?></strong>
                    <a href="<?= BASEURL?>/index.php/order_items/add/<?= $product['id']?>">[acheter]</a>
                </div>
            </div>
            <br>
           
            
            <?php
        } 
    ?>
</section>