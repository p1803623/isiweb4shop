<?php
class panierController extends Controller 
{
    public function index(){
        $data['title'] = 'Panier';

        $this->view('template/header', $data);

        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);


        if(isset($_SESSION['Id'])){
            $idOrder = $this->model('ordersModel')->isOrdering($_SESSION['Id']);
            if($idOrder != null){
                $_SESSION['SESS_ORDENUM'] = $idOrder;
            }
        }

        if(!isset($_SESSION['SESS_ORDENUM'])){
            $this->redirect('index.php/order_items');
        }

        $status = $this->model('ordersModel')->getStatus($_SESSION['SESS_ORDENUM']);

        if($status == 0 || $status == 10){
            $this->redirect('index.php/order_items');
        } 

        if($status == 1){
            $this->view('payment/payment');
        }

        if($status == 2){
            $this->view('payment/attente');
        }

        $this->view('template/footer');
    }

    public function payment($paymentType){
        if(!isset($_SESSION['SESS_ORDENUM'])){
            $this->redirect('index.php/error/index/pas_de_panier');
        }

        $status = $this->model('ordersModel')->getStatus($_SESSION['SESS_ORDENUM']);
        if($status != 1){
            $this->redirect('index.php/error/index/pas_d_adresse_disponible');
        }

        $this->model('ordersModel')->changeStatut($_SESSION['SESS_ORDENUM'], 2);
        $this->model('ordersModel')->changePaymentType($_SESSION['SESS_ORDENUM'], $paymentType);

        if($paymentType == 1){
            // payment paypal
            header('Location: https://www.paypal.com/fr/home');
        }

        if($paymentType == 2){
            // payment cheque
            $total = $this->model('ordersModel')->getTotal($_SESSION['SESS_ORDENUM']);
            
            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial','B',16);
            $pdf->Cell(0,20,"Facture",0,1,"C");

            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(0,10,"Vous devez un montant de : $total",0,1);
            

            $pdf->Cell(0,20,"Vous ferez ce cheque a l'ordre : isiwebshop",0,1);
            $pdf->Cell(0,10,"Et le posterez a l'adresse :",0,1);
            $pdf->Cell(0,5,"15 Boulevard Andre Latarjet, 69100 Villeurbanne",0,1);
            $pdf->Output();
        }

    }

    public function validation($idOrder){
        $this->model('ordersModel')->changeStatut($idOrder, 10);
        $this->redirect('index.php/admin');
    }
}  
?> 