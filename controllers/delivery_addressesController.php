<?php
class delivery_addressesController extends Controller 
{
    public function index(){
        $data['title'] = 'Adresse';

        $this->view('template/header', $data);

        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);

        if(isset($_SESSION['Id'])){
            $idAddress = $this->model('ordersModel')->getIdAddress($_SESSION['SESS_ORDENUM']);
            $address = $this->model('delivery_addressesModel')->getAddress($idAddress);
            $this->view('delivery_adresses/update', $address[0]);
        }
        else{
            $this->view('delivery_adresses/create');
        }
        $this->view('template/footer');
    }

    public function add(){
        if(!isset($_POST['confirmer'])){
            $this->redirect('index.php/error/index/noFormulaire');
        }
        
        $address['prenom'] = $_POST['prenom'];
        $address['nom'] = $_POST['nom'];
        $address['address1'] = $_POST['address1'];
        $address['postCode'] = $_POST['postcode'];
        $address['phone'] = $_POST['phone'];
        $address['city'] = $_POST['city'];
        $address['email'] = $_POST['email'];

        $id = $this->model('delivery_addressesModel')->add($address);
        $this->model('ordersModel')->updateAddressId($_SESSION['SESS_ORDENUM'], $id);
        $this->model('ordersModel')->changeStatut($_SESSION['SESS_ORDENUM'], 1);

        $this->redirect('index.php/panier');
    }

    public function update(){
        if(!isset($_POST['confirmer'])){
            $this->redirect('index.php/error/index/404');
        }
        
        $address['prenom'] = $_POST['prenom'];
        $address['nom'] = $_POST['nom'];
        $address['address1'] = $_POST['address1'];
        $address['postCode'] = $_POST['postcode'];
        $address['phone'] = $_POST['phone'];
        $address['city'] = $_POST['city'];
        $address['email'] = $_POST['email'];


        $idAddress = $this->model('ordersModel')->getIdAddress($_SESSION['SESS_ORDENUM']);
        $this->model('delivery_addressesModel')->update($address, $idAddress);
        $this->model('ordersModel')->changeStatut($_SESSION['SESS_ORDENUM'], 1);

        $this->redirect('index.php/panier');
    }
}  
?> 