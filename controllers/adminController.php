<?php
class adminController extends Controller 
{
    public function index($idOrder = 0){
        $data['title'] = 'Admin';

        $this->view('template/header', $data);

        if(isset($_SESSION['admin'])){
            $orders = $this->model('ordersModel')->getOrders();

            if($idOrder != 0){
               $order_items = $this->model('order_itemsModel')->getItems($idOrder);
               $idAddress = $this->model('ordersModel')->getIdAddress($idOrder);
               $address = $this->model('delivery_addressesModel')->getAddress($idAddress);
               $data['order']['items'] = $order_items;
               $data['order']['address'] = $address[0];
            }

            $data['orders'] = $orders;

            $this->view('admin/panel', $data);
        }
        else{
            $this->view('admin/connexion');
        }
        $this->view('template/footer');
    }

    public function connexion(){
        session_unset();

        $username = $_POST["username"];
        $pass = $_POST["password"];
        $idLogin = $this->model('adminModel')->isExist($username,$pass);
        if ($idLogin != NULL)
        {
            $_SESSION["admin"] = $idLogin;
        }
        
        $this->redirect("index.php/admin");
    }

    public function deconnexion()
    {
        session_unset();
        $this->redirect("index.php/admin");
    }
}  
?> 