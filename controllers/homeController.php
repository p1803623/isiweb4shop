<?php
class homeController extends Controller 
{
    public function index(){
        $data['title'] = 'home';

        $this->view('template/header', $data);

        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);


        $this->view('home/home');
        $this->view('template/footer');
    }
}  
?> 