<?php
class errorController extends Controller 
{
    public function index($message){
        $data['title'] = 'error';

        $this->view('template/header', $data);

        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);


        $this->view('error/error', $message);
        $this->view('template/footer');
    }
}  
?> 