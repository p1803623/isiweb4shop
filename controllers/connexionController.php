<?php
class connexionController extends Controller 
{
    public function index(){
        $data['title'] = 'Connexion';

        $this->view('template/header', $data);

        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);


        $this->view('connexion/connexion');
        $this->view('template/footer');
    }
    public function connexion(){
        session_unset ();

        $username = $_POST["username"];
        $pass = $_POST["password"];
        $idLogin = $this->model('LoginModel')->isExist($username,$pass);
        if ($idLogin != NULL)
        {
            $_SESSION["Id"] = $idLogin;
            $_SESSION["Username"] = $username;
            $this->redirect("index.php");
        }
        else{
            $this->redirect("index.php/connexion");
        }

    }

    public function deconnexion()
    {
        session_unset ();
        $this->redirect("index.php");
    }
}  
?> 