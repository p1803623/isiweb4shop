<?php
class order_itemsController extends Controller 
{

    public function index(){
        $data['title'] = 'Panier';
        
        $this->view('template/header', $data);
        
        
        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);

        if(!isset($_SESSION['SESS_ORDENUM'])){
            $data = [];
        }
        else{
            $order_items = $this->model('order_itemsModel')->getItems($_SESSION['SESS_ORDENUM']);
    
            $data = [];
    
            for($i= 0; $i<sizeof($order_items); $i++){
                $product = $this->model('ProductsModel')->getProduct($order_items[$i]['product_id']);
                $data[$i]['id'] = $order_items[$i]['product_id']; 
                $data[$i]['quantity'] = $order_items[$i]['quantity'];
                $data[$i]['name'] = $product['name'];
                $data[$i]['image'] = $product['image'];
                $data[$i]['price'] = $product['price']; 
            }
        }
        

        $this->view('order_items/index', $data);
        $this->view('template/footer');
    }


    public function add($productId){

       
        if(!isset($_SESSION['SESS_ORDENUM'])){
            if(isset($_SESSION['Id'])){
                // un utilisateur est connecté 
                $idOrder = $this->model('ordersModel')->isOrdering($_SESSION['Id']);

                if($idOrder == null){
                    // pas de commande en cours
                    $idAddress = $this->model('customersModel')->getIdAddress($_SESSION['Id']); 
                    $idOrder = $this->model('ordersModel')->add($_SESSION['Id'], true, $idAddress);
                }
                else{
                    // commande en cours 
                    $status = $this->model('ordersModel')->getStatus($_SESSION['SESS_ORDENUM']);
                    if($status != 0){
                        // la commande est en cours de payment
                        $this->redirect('index.php/error/index/commande_en_cours');
                    }
                }

                $_SESSION['SESS_ORDENUM'] = $idOrder;
            }
            else{
                $idOrder = $this->model('ordersModel')->add(session_id(), false);
                $_SESSION['SESS_ORDENUM'] = $idOrder;
            }
        }

        $status = $this->model('ordersModel')->getStatus($_SESSION['SESS_ORDENUM']);
        if($status != 0){
            // la commande est en cours de payment
            $this->redirect('index.php/error/index/commande_en_cours');
        }
 
        $this->model('order_itemsModel')->add($productId, $_SESSION['SESS_ORDENUM']);
        $product = $this->model('ProductsModel')->getProduct($productId);
        $this->model('ordersModel')->addTotal($_SESSION['SESS_ORDENUM'], $product['price']);


        $this->redirect('index.php/order_items');
    }


    public function delete($id){
       if(!isset($_SESSION['SESS_ORDENUM'])){
           $this->redirect('index.php/error/index/404');
       }

       $this->model('order_itemsModel')->delete($_SESSION['SESS_ORDENUM'], $id);
       $this->redirect('index.php/panier');
    }
}  
?> 