<?php
class productsController extends Controller 
{

    public function index($catId = 0){
        $data['title'] = 'Products';
        
        $this->view('template/header', $data);
        
        
        $categories = $this->model('CategoriesModel')->getCategories();
        $this->view('template/aside', $categories);
        if($catId == 0){
            $products = $this->model('ProductsModel')->getProducts();
        }
        else{
            $products = $this->model('ProductsModel')->getProducts($catId);
        }
        
        
        $this->view('products/index', $products);
        $this->view('template/footer');
    }
}  
?> 