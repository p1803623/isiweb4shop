<?php
    class order_itemsModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function getItems($idOrder){
            $this->db->open_db();


            $this->db->query("SELECT * FROM orderitems WHERE order_id = :idOrder");
            $this->db->bind('idOrder', $idOrder);
            
            $order_items = $this->db->resultSet();

            $this->db->close_db();
            return $order_items;
        }

        public function add($productId, $orderId){
            $this->db->open_db();
            $quantity = $this->getQuantity($productId, $orderId);

            if($quantity != 0){
                $this->db->query("UPDATE orderitems SET quantity = :quantity WHERE order_id = :orderID AND product_id = :pID");
                $this->db->bind('pID', $productId);
                $this->db->bind('orderID', $orderId);
                
                $quantity++;
                $this->db->bind('quantity', $quantity);
            }
            else{
                $this->db->query("INSERT INTO `orderitems`(`order_id`, `product_id`, `quantity`) VALUES (:orderID, :pID, 1)");
                $this->db->bind('pID', $productId);
                $this->db->bind('orderID', $orderId);
            }

            $this->db->execute();
            $this->db->close_db();
        }

        public function getQuantity($productId, $orderId){
            $this->db->open_db();

            $this->db->query("SELECT quantity FROM orderitems WHERE product_id = :pID AND order_id = :orderID");
            $this->db->bind('pID', $productId);
            $this->db->bind('orderID', $orderId);

            $res = $this->db->single();

            $this->db->close_db();
            
            if($res != null){
                return $res['quantity'];
            }
            else{
                return 0;
            }
        }

        public function delete($orderId, $productId){
            $this->db->open_db();
                $quantity = $this->getQuantity($productId, $orderId);

                if($quantity > 1){
                    // do quantity - 1
                    $this->db->query("UPDATE orderitems SET quantity = :quantity WHERE order_id = :orderID AND product_id = :pID");
                    $this->db->bind('pID', $productId);
                    $this->db->bind('orderID', $orderId);
                    
                    $quantity--;
                    $this->db->bind('quantity', $quantity);
                }
                else{
                    // delete product 
                    $this->db->query("DELETE FROM `orderitems` WHERE order_id = :orderID AND product_id = :pID");
                    $this->db->bind('pID', $productId);
                    $this->db->bind('orderID', $orderId);
                }

                $this->db->execute();
                $this->db->close_db();
        }

    }

?>