<?php
    class ordersModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function getOrders(){
            $this->db->open_db();


            $this->db->query("SELECT * FROM orders");
            
            $orders = $this->db->resultSet();

            $this->db->close_db();
            return $orders;
        }


        public function add($customer_id, $registered, $addressId = 0){
            $this->db->open_db();
            if($registered == true){
                $this->db->query("INSERT INTO `orders`(`customer_id`, `registered`, `delivery_add_id`, `payment_type`, `date`, `status`, `session`, `total`) 
                                    VALUES (:customer_id,1, :addressID ,0,NOW(),0,'',0)");
                $this->db->bind('addressID', $addressId);
            }
            else{
                $this->db->query("INSERT INTO `orders`(`customer_id`, `registered`, `delivery_add_id`, `payment_type`, `date`, `status`, `session`, `total`) 
                                    VALUES (0,0,0,0,NOW(),0,:customer_id,0)");
            }

            $this->db->bind('customer_id', $customer_id);
            $this->db->execute();

            // get orderId insert
            $this->db->query("SELECT MAX(id) AS LastID FROM orders");
            $res = $this->db->single();

            $this->db->close_db();

            return $res['LastID'];
           
        }

        public function isOrdering($customer_id){
            $this->db->open_db();
            $this->db->query("SELECT id FROM orders WHERE customer_id = :customID AND `status` != 10");
            $this->db->bind('customID', $customer_id);
            $res = $this->db->single();
            $this->db->close_db();
            
            if($res != NULL){
                return $res['id'];
            }
            else{
                return null;
            }
        }

        public function getStatus($idOrder){
            $this->db->open_db();
            $this->db->query("SELECT `status` FROM orders WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $res = $this->db->single();
            $this->db->close_db();
            
            if($res != NULL){
                return $res['status'];
            }
            else{
                return false;
            }
        }

        public function updateAddressId($idOrder, $idAddress){
            $this->db->open_db();
            $this->db->query("UPDATE `orders` SET `delivery_add_id`= :idAddress WHERE id=:idOrder");
            $this->db->bind('idAddress', $idAddress);
            $this->db->bind('idOrder', $idOrder);
            $this->db->execute();
            $this->db->close_db();
        }

        public function changeStatut($idOrder, $newStatus){
            $this->db->open_db();
            $this->db->query("UPDATE `orders` SET `status`= :newStatus WHERE id=:idOrder");
            $this->db->bind('newStatus', $newStatus);
            $this->db->bind('idOrder', $idOrder);
            $this->db->execute();
            $this->db->close_db();
        }

        public function getIdAddress($idOrder){
            $this->db->open_db();
            $this->db->query("SELECT `delivery_add_id` FROM orders WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $res = $this->db->single();
            $this->db->close_db();
            
            if($res != NULL){
                return $res['delivery_add_id'];
            }
            else{
                return false;
            }
        }

        public function changePaymentType($idOrder, $paymentType){
            $this->db->open_db();
            $this->db->query("UPDATE `orders` SET `payment_type`= :paymentType WHERE id=:idOrder");
            $this->db->bind('paymentType', $paymentType);
            $this->db->bind('idOrder', $idOrder);
            $this->db->execute();
            $this->db->close_db();
        }

        public function addTotal($idOrder, $price2add){
            $this->db->open_db();
            $this->db->query("SELECT `total` FROM orders WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $res = $this->db->single();
            
            $total = $res['total'] + $price2add;
            $this->db->query("UPDATE `orders` SET `total` = :total WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $this->db->bind('total', $total);

            $this->db->execute();
            $this->db->close_db();
        }

        public function getTotal($idOrder){
            $this->db->open_db();
            $this->db->query("SELECT `total` FROM orders WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $res = $this->db->single();
            $this->db->close_db();
            
            if($res != NULL){
                return $res['total'];
            }
            else{
                return false;
            }
        }

        
    }

?>