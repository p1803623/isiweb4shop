<?php
    class ProductsModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function getProducts($idCat = 0){
            $this->db->open_db();

            if($idCat == 0){
                $this->db->query("SELECT * FROM products");
            }
            else{
                $this->db->query("SELECT * FROM products WHERE cat_id = :catId");
                $this->db->bind('catId', $idCat);
            }
            $products = $this->db->resultSet();

            $this->db->close_db();
            return $products;
        }


        public function getProduct($idProduct){
            $this->db->open_db();
            $this->db->query("SELECT * FROM products WHERE id = :Id");
            $this->db->bind('Id', $idProduct);


            $product = $this->db->single();

            $this->db->close_db();
            return $product;
        }
    }

?>