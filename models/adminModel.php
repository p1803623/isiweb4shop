<?php
    class adminModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function isExist($username,$password){
            $this->db->open_db();
            $this->db->query("SELECT id FROM `admin` WHERE username = :user AND password = :pass");
            $this->db->bind("user", $username);
            $this->db->bind("pass", sha1($password));
            
            $res = $this->db->single();
            $this->db->close_db();

            if ( $res["id"] != NULL)
            {   
                return $res["id"];
            }
            return NULL;
        }

    }

?>