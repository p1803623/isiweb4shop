<?php
    class Delivery_addressesModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function add($address){
            $this->db->open_db();
            $this->db->query("INSERT INTO `delivery_addresses`(`firstname`, `lastname`, `add1`, `add2`, `city`, `postcode`, `phone`, `email`) 
                            VALUES (:firstName, :lastName, :address1, '', :city, :postCode, :phone, :email)");

            $this->db->bind('firstName', $address['prenom']);
            $this->db->bind('lastName', $address['nom']);
            $this->db->bind('address1', $address['address1']);
            $this->db->bind('city', $address['city']);
            $this->db->bind('postCode', $address['postCode']);
            $this->db->bind('phone', $address['phone']);
            $this->db->bind('email', $address['email']);
            
            $this->db->execute();
            
            // get delivery_address id insert
            $this->db->query("SELECT MAX(id) AS LastID FROM delivery_addresses");
            $res = $this->db->single();

            $this->db->close_db();

            return $res['LastID'];
        }


        public function getAddress($idOrder){
            $this->db->open_db();
            $this->db->query("SELECT * FROM delivery_addresses WHERE id = :ID ");
            $this->db->bind('ID', $idOrder);
            $address = $this->db->resultSet();

            $this->db->close_db();
            return $address;
        }

        public function update($address, $idAddress){
            $this->db->open_db();
            $this->db->query("UPDATE `delivery_addresses` 
                                SET `firstname`= :firstName ,`lastname`= :lastName,`add1`= :address1,`add2`= '', `city`= :city,`postcode`= :postCode,`phone`= :phone, `email`= :email 
                                WHERE `id` = :idAddress");

            $this->db->bind('idAddress', $idAddress);
            $this->db->bind('firstName', $address['prenom']);
            $this->db->bind('lastName', $address['nom']);
            $this->db->bind('address1', $address['address1']);
            $this->db->bind('city', $address['city']);
            $this->db->bind('postCode', $address['postCode']);
            $this->db->bind('phone', $address['phone']);
            $this->db->bind('email', $address['email']);
            
            $this->db->execute();
        }


    }

?>