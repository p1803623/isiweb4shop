<?php
    class customersModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function getIdAddress($idCustomer){
            $this->db->open_db();


            $this->db->query("SELECT id_address FROM customers WHERE id = :idCustomer");
            $this->db->bind('idCustomer', $idCustomer   );
            
            $idAddress = $this->db->single();

            $this->db->close_db();
            return $idAddress['id_address'];
        }

    }

?>