<?php
    class CategoriesModel
    {
        private $db;

        public function __construct()
        {
            $this->db = new BD();
        }

        public function getCategories(){
            $this->db->open_db();
            $this->db->query("SELECT * FROM categories");
            $categories = $this->db->resultSet();

            $this->db->close_db();
            return $categories;
        }
    }

?>