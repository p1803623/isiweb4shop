# ISIWEB4Shop

Site shopping bag réalisé dans le cadre de l'ue ISIWEB par ABDELGHANI Tarek et BAUVENT Kilian 

Pour lancer le site de votre côté :
- utiliser wamp ou xamp 
- insérer la base de donnée du dossier bd dans votre phpmyadmin 
- changer les valeurs du fichier core/config.php en fonction des paramètres de votre base de donnée et de l'url de base de votre projet 
- lancer l'index.php 

Vous pouvez tester le site avec un utilisateur en utilisant le compte :
nom : Hamidou
mot de passe : Hamidou+123

Vous pouvez tester les possibilité administrateur en allant sur l'url : index.php/admin
Et en vous connectant avec le compte :
nom : John
mot de passe : John+123