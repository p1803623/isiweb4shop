<?php  
class App    
{
    protected $controller = 'home';
    protected $method = 'index';
    protected $param = [];

    function __construct() {   
        $parseURL = $this->parseURL();
        if(isset($parseURL[3])){
            if (file_exists('./controllers/'. $parseURL[3] . 'Controller.php')){
                $this->controller = $parseURL[3] . 'Controller';
            }
            else{
                $this->controller = 'homeController';
            }
        }
        else{
            $this->controller = 'homeController';
        } 

        require_once './controllers/'.$this->controller.'.php';
        $this->controller = new $this->controller;

        if(isset($parseURL[4])){
            if (method_exists($this->controller, $parseURL[4])) {
                $this->method = $parseURL[4];
            }
            else{
                $this->method = 'index';
            }
        }
        else{
            $this->method = 'index';
        }

        if(isset($parseURL[5])){
            $this->param[0] = $parseURL[5];
        }
        

    } 

    function parseURL()
	{
		$url = explode('/', filter_var(trim($_SERVER['REQUEST_URI']), FILTER_SANITIZE_URL));

		return $url;
    }

    function run(){
        return call_user_func_array([$this->controller, $this->method], $this->param);
    }
    
}  
?> 