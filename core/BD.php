<?php  
class BD    
{
    private $host;
    private $user;
    private $pass;
    private $db;

    private $cnx;
    private $stmt;

    
    
    function __construct() {  
        $this->host = DB_HOST;  
        $this->user  = DB_USER;  
        $this->pass = DB_PASS;  
        $this->db = DB_NAME;  
    } 
    
    function open_db()
    {
        try {
            $this->cnx = new PDO("mysql:host=$this->host;dbname=$this->db", $this->user, $this->pass);
            $this->cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->cnx->exec('SET CHARACTER SET utf8');
        } catch (PDOException $e) {
            $erreur = $e->getMessage();
            echo $erreur;
        }
        return $this->cnx;
    }

    function close_db()
    {
        try {
            $cnx = null;
        } catch (PDOException $e) {
            $erreur = $e->getMessage();
        }
    }

    public function query($query)
	{
		$this->stmt = $this->cnx->prepare($query);
    }
    
    public function execute()
	{
		$this->stmt->execute();
    }

    public function bind($param, $value, $type = null)
	{
		if(is_null($type)){
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
					
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;

				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;

				default:
					$type = PDO::PARAM_STR;
			}
		}

		return $this->stmt->bindParam($param, $value, $type);
    }
    
	public function resultSet()
	{
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function single()
	{
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	} 
}  
?> 